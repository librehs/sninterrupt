lookDomains = {
  "wikipedia.org": "https://wikipedia.org/gfw"
};

lookKeys = Object.keys(lookDomains);

waitingDomains = {};

function getHostname(url) {
  var rgx = /https?:\/\/(.*?)\//;
  var rgxResult = rgx.exec(url);
  if (rgxResult && rgxResult[1]) {
    return rgxResult[1];
  } else {
    return false;
  }
}

function domainMatch(host) {
  if (!host) return false;
  for (i of lookKeys) {
    if (host.search(i) != -1) {
      return lookDomains[i];
    }
  }
  return false;
}

browser.tabs.onUpdated.addListener((tabId, changeInfo, tabInfo) => {
  if (changeInfo.status == "loading") return;
  let url = tabInfo.url;
  if (waitingDomains[tabId]) {
    // Redirect to the pre-redirect page.
    var updating = browser.tabs.update(tabId, {
      url: waitingDomains[tabId]
    });
    waitingDomains[tabId] = undefined;
    var settingIcon = browser.browserAction.setIcon({
      path: {
        32: "image/32-blue.png"
      }
    });
    return;
  }
  var settingIcon = browser.browserAction.setIcon({
    path: {
      32: "image/32-normal.png"
    }
  });
});

browser.browserAction.onClicked.addListener(tab => {
  let host = getHostname(tab.url);
  matchResult = domainMatch(host);
  if (matchResult) {
    var updating = browser.tabs.update(tab.id, {
      url: matchResult
    });
    waitingDomains[tab.id] = tab.url;
  }
});
