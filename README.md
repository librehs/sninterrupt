# SNInterrupt

SNI (Server Name Indication) is not really safe. Let's use session ids or session tickets to bypass it.

**It hasn't been submitted to AMO (addons.mozilla.org) and hasn't been signed. You may install the extension in [another way](https://gitlab.com/librehs/sninterrupt/wikis/install-an-unsigned-extension).**

**You can also download [the xpi file](https://tghost.me/5O0fR8pmjupx2O6) (version [1.0.1](https://gitlab.com/librehs/sninterrupt/commit/a5d0e9d7c593cbcae6f2549a38d88bc5a4991465)) signed by [zssrnsy](https://cmx.im/@zssrnsy)**.

## Usage
When blocked from visiting one of the listed websites:

* Click the green "-SNI" button
* Wait for a redirect
* Automatically back, and it works!

## Currently working on
* *.wikipedia.org